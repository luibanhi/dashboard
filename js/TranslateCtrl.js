angular.module('inspinia').controller('translateCtrl', ['$translate', '$scope', function($translate, $scope){
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
    };
}]);