angular.module('inspinia').controller('dashboardUserCtrl', ['$scope', '$http' ,'$rootScope', function($scope, $http, $rootScope){
    $rootScope.mockData = {};
    this.mockDados = function(){
            $http({
                method: 'GET',
                url: 'https://testando-fc9aa.firebaseio.com/mockCIEP.json',
            }).success(function(res) {
                $rootScope.mockDados = res['data'];
                $scope.data1 = res['data1'];
                $scope.data2 = res['data2'];
                $scope.flotData = [$scope.data1, $scope.data2];
                //$scope.labels = ["Janeiro", "Feveireiro", "Março", "Abril", "Maio", "Junho", "Julho"];    
                //$scope.series = ['Cliques', 'Comentários '];
            })
    } 

    $scope.options = {
        series: {
            lines: {
                show: false,
                fill: true
            },
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.4
            },
            points: {
                radius: 0,
                show: true
            },
            shadowSize: 2
        },
        grid: {
            hoverable: true,
            clickable: true,

            borderWidth: 2,
            color: 'transparent'
        },
        colors: ["#1ab394", "#1C84C6"],
        xaxis:{
        },
        yaxis: {
        },
        tooltip: false
    };

    /**
     * Definition of variables
     * Flot chart
     */


    var sparkline1Data = [34, 43, 43, 35, 44, 32, 44, 52];
    var sparkline1Options = {
        type: 'line',
        width: '100%',
        height: '50',
        lineColor: '#1ab394',
        fillColor: "transparent"
    };

    var sparkline2Data = [32, 11, 25, 37, 41, 32, 34, 42];
    var sparkline2Options = {
        type: 'line',
        width: '100%',
        height: '50',
        lineColor: '#1ab394',
        fillColor: "transparent"
    };

    this.sparkline1 = sparkline1Data;
    this.sparkline1Options = sparkline1Options;
    this.sparkline2 = sparkline2Data;
    this.sparkline2Options = sparkline2Options;
}]);